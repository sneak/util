# sneak/goutil

This is a bunch of little general-purpose go utility functions that I don't
feel comfortable including as-is in my normal projects as they are too
general, so I put them here.

Suggestions are welcome.

Really I think most of these should probably be in the stdlib.

# License

WTFPL (free software, no restrictions)



# Author

* [sneak@sneak.berlin](mailto:sneak@sneak.berlin)

